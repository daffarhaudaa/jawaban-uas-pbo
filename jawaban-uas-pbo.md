# 1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital

- Use Case User

| Use Case | Nilai |
| ------ | ------ |
|1. Membuat Folder        |     90    |
|2. Mengedit File        |    80    |
|3. Mengelola Akses      |    75     |
|4. Mengunggah, Mengedit, dan Menghapus|    75|
|5. Penyimpanan | 70 |
|6. Menghapus Folder | 60 |
|7. Mengunduh File | 50 |

- Use Case Manajemen Perusahaan

| Use Case | Nilai |
| ------ | ------ |
|1. Mengelola Akses        |    90    |

- Use Case Direksi Perusahaan (Dashboard, Monitoring, Analisis)

| Use Case | Nilai |
| ------ | ------ |
|1. Mengedit Akun User | 90 |
|2. Membuat Folder        |     90    |
|3. Mengedit File        |    80    |
|4. Mengelola Akses      |    75     |
|5. Mengunggah, Mengedit, dan Menghapus|    75|
|6. Penyimpanan | 70 |
|7. Menghapus Folder | 60 |
|8. Mengunduh File | 50 |

# 2. Mampu mendomenstrasikan Class Diagram dari keseluruhan Use Case produk digital
```mermaid
classDiagram
    class User {
        +username: string
        +uploadFile(file: File): void
        +downloadFile(file: File): void
        +deleteFile(file: File): void
        +shareFile(file: File, users: User[]): void
        +createFolder(folder: Folder): void
        +deleteFolder(folder: Folder): void
        +shareFolder(folder: Folder, users: User[]): void
    }

    class File {
        +name: string
        +size: number
    }

    class Folder {
        +name: string
        +files: File[]
        +folders: Folder[]
    }

    User --|> File
    User --|> Folder
    Folder "1" --> "0..*" File
    Folder "1" --> "0..*" Folder

```

# 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
Dalam kode di atas, SOLID Design Principles dapat diterapkan sebagai berikut:
1. Single Responsibility Principle (SRP):
Setiap kelas memiliki tanggung jawab tunggal yang terdefinisi dengan jelas. Misalnya, kelas `User` bertanggung jawab atas tindakan pengguna terkait file dan folder, dan kelas `FileSizeCalculator` bertanggung jawab menghitung ukuran file.
```
<?php
// File Size Calculator
interface FileSizeCalculatorInterface
{
    public function getUsedSpace();
}

class FileSizeCalculator implements FileSizeCalculatorInterface
{
    private $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    public function getUsedSpace()
    {
        $folders = array_filter(glob($this->directory . '*'), 'is_dir');
        $totalSize = 0;

        foreach ($folders as $folder) {
            $files = glob($folder . '/*');
            foreach ($files as $file) {
                $totalSize += filesize($file);
            }
        }

        return $totalSize;
    }
}
```

2. Open/Closed Principle (OCP):
Kode di atas mengikuti prinsip OCP dengan menggunakan polimorfisme dan pewarisan. Misalnya, kelas `User` dan `kelas Folder` dapat diperluas dengan menambahkan metode atau atribut baru tanpa mempengaruhi kelas lain.
```
$directory = 'uploads/';
$fileSizeCalculator = new FileSizeCalculator($directory);
$usedSpace = $fileSizeCalculator->getUsedSpace();
$usedSpaceFormatted = FileSizeFormatter::formatSizeUnits($usedSpace);
?>
```

3. Liskov Substitution Principle (LSP):
Pada kode di atas, prinsip LSP tidak secara eksplisit diimplementasikan, tetapi jika ada kelas turunan dari kelas utama seperti `User`, kelas turunan tersebut harus dapat digunakan sebagai pengganti kelas utama tanpa mengubah fungsionalitas yang diharapkan.
4. Interface Segregation Principle (ISP):
Tidak ada interface yang terlihat dalam kode tersebut, tetapi jika ada, prinsip ISP dapat diterapkan dengan membagi antarmuka yang spesifik ke setiap klien, sehingga klien hanya bergantung pada metode yang relevan bagi mereka.
5. Dependency Inversion Principle (DIP):
Dalam kode di atas, DIP diterapkan dengan menggunakan kelas `FileSizeCalculatorInterface` sebagai kontrak antarmuka yang diterapkan oleh kelas `FileSizeCalculator`. Ini memungkinkan penggunaan kelas `FileSizeCalculator` secara fleksibel dalam menghitung ukuran file.
```
class FileSizeCalculator implements FileSizeCalculatorInterface
{
    private $directory;

    public function __construct($directory)
    {
        $this->directory = $directory;
    }

    public function getUsedSpace()
    {
        $folders = array_filter(glob($this->directory . '*'), 'is_dir');
        $totalSize = 0;

        foreach ($folders as $folder) {
            $files = glob($folder . '/*');
            foreach ($files as $file) {
                $totalSize += filesize($file);
            }
        }

        return $totalSize;
    }
}
```

# 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

- Singleton Pattern:
Design pattern Singleton digunakan untuk memastikan bahwa hanya ada satu instance dari sebuah kelas yang dapat diakses secara global. Dalam kode tersebut, terdapat penggunaan Singleton pada kelas `FileSizeCalculator`. Singleton digunakan untuk memastikan bahwa hanya ada satu objek `FileSizeCalculator` yang digunakan untuk menghitung ruang yang digunakan oleh file.
```
$fileSizeCalculator = new FileSizeCalculator($directory);
$usedSpace = $fileSizeCalculator->getUsedSpace();
$usedSpaceFormatted = FileSizeFormatter::formatSizeUnits($usedSpace);
?>
```
- Factory Pattern:
Factory pattern adalah design pattern creational yang digunakan untuk membuat objek tanpa mengekspos logika pembuatannya. Dalam kode tersebut, terdapat penggunaan Factory Pattern dalam pembuatan instance objek `FileSizeCalculator`. Dengan menggunakan Factory Pattern, Anda dapat membuat instance `FileSizeCalculator` melalui factory tanpa perlu mengekspos logika pembuatannya secara langsung.

- Strategy Pattern:
Strategy pattern adalah design pattern behavioral yang memungkinkan pengguna untuk memilih satu dari beberapa algoritma yang berbeda saat runtime. Dalam kode tersebut, terdapat penggunaan Strategy Pattern dalam fungsi `getPreviewIcon` dan `getPreviewAction`. Kedua fungsi tersebut mengembalikan ikon dan tindakan berdasarkan tipe file yang diberikan. Dengan menggunakan Strategy Pattern, Anda dapat dengan mudah mengubah atau menambahkan strategi baru tanpa mengubah struktur yang ada.
```
$previewIcon = getPreviewIcon($fileType);
          $previewAction = getPreviewAction($fileType, $directory . $filePath);

          echo '<tr>';
          echo '<td>' . $file . '</td>';
          echo '<td>' . $folderName . '</td>';
          echo '<td><i class="' . $previewIcon . '"></i></td>';
          echo '<td>';
          echo $previewAction;
          echo '<a href="hapus.php?file=' . urlencode($filePath) . '" class="btn btn-danger-custom"><i class="fas fa-trash"></i> Hapus</a> ';
          echo '<button class="btn btn-primary-custom" data-toggle="modal" data-target="#changeFileModal" data-file="' . $filePath . '"><i class="fas fa-exchange-alt"></i> Ganti</button>';
          echo '</td>';
          echo '</tr>';
        }
      }
    }

    function getPreviewIcon($fileType) {
      if (strpos($fileType, 'image') !== false) {
        return 'fas fa-eye';
      } elseif (strpos($fileType, 'pdf') !== false) {
        return 'fas fa-file-pdf';
      } elseif (strpos($fileType, 'msword') !== false || strpos($fileType, 'vnd.openxmlformats-officedocument.wordprocessingml.document') !== false) {
        return 'fas fa-file-word';
      } else {
        return 'fas fa-file';
      }
    }

    function getPreviewAction($fileType, $filePath) {
      $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
```
- Observer Pattern:
Observer pattern adalah design pattern behavioral yang digunakan untuk mengatur hubungan antara objek-objek yang bergantung pada satu objek subjek. Dalam kode tersebut, tidak terlihat secara langsung, tetapi penggunaan event handling atau callback dapat dikategorikan sebagai Observer Pattern. Misalnya, saat tombol "Upload" ditekan, fungsi `$_POST['upload']` dijalankan untuk memperbarui tampilan atau melakukan tindakan lain.
```
<?php
// Fungsi Upload File
if (isset($_POST['upload'])) {
  $file = $_FILES['file'];
  $fileName = $file['name'];
  $fileTmp = $file['tmp_name'];
  $fileSize = $file['size'];
  $fileError = $file['error'];
  $folder = $_POST['folder'];

  if ($fileError === 0) {
    $destination = $directory . $folder . '/' . $fileName;
    move_uploaded_file($fileTmp, $destination);
    echo '<div class="alert alert-success">File berhasil diupload.</div>';
  } else {
    echo '<div class="alert alert-danger">Terjadi kesalahan saat mengupload file.</div>';
  }
}
?>
```

# 5. Mampu menunjukkan dan menjelaskan konektivitas ke database
![dokumentasi](dokumentasi/koneksi.jpg)

Kode di atas berfungsi untuk melakukan otentikasi pengguna dengan memeriksa username dan password yang diberikan melalui formulir login dengan data yang ada di tabel "user" dalam database.

Dengan menambahkan langkah-langkah di atas,akan memiliki koneksi database yang aktif yang dapat digunakan untuk menjalankan kueri otentikasi dan memeriksa apakah pengguna berhasil masuk atau tidak.

# 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

- Web Service yang dipakai
> Slim FrameWork V3
- Web Page untuk User
![dokumentasi](dokumentasi/webpage.jpg)

- Web Service Untuk Aplikasi
![dokumentasi](dokumentasi/webservice.jpg)

- Tampilan
![dokumentasi](dokumentasi/crud.jpg)

CRUD disini berfungsi untuk mengupload, mengedit, menghapus file.

```
    <!-- Form Upload -->
<h4 id="upload">Upload File</h4>
<form action="" method="POST" enctype="multipart/form-data">
  <div class="form-group">
    <label for="file">Pilih File:</label>
    <input type="file" name="file" id="file" class="form-control-file" required>
  </div>
  <div class="form-group">
    <label for="folder">Pilih Folder:</label>
    <select name="folder" id="folder" class="form-control">
      <?php
      $folders = array_filter(glob($directory . '*'), 'is_dir');
      foreach ($folders as $folder) {
        $folderName = basename($folder);
        echo '<option value="' . $folderName . '">' . $folderName . '</option>';
      }
      ?>
    </select>
  </div>
  <input type="submit" name="upload" value="Upload" class="btn btn-primary-custom">
</form>

<?php
// Fungsi Upload File
if (isset($_POST['upload'])) {
  $file = $_FILES['file'];
  $fileName = $file['name'];
  $fileTmp = $file['tmp_name'];
  $fileSize = $file['size'];
  $fileError = $file['error'];
  $folder = $_POST['folder'];

  if ($fileError === 0) {
    $destination = $directory . $folder . '/' . $fileName;
    move_uploaded_file($fileTmp, $destination);
    echo '<div class="alert alert-success">File berhasil diupload.</div>';
  } else {
    echo '<div class="alert alert-danger">Terjadi kesalahan saat mengupload file.</div>';
  }
}
?>

<!-- Daftar File -->
<h2 id="daftar-file" class="mt-4-custom">Daftar File</h2>
<table class="table table-custom">
  <thead>
    <tr>
      <th>Nama File</th>
      <th>Folder</th>
      <th>Pratinjau</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $folders = array_filter(glob($directory . '*'), 'is_dir');
    foreach ($folders as $folder) {
      $folderName = basename($folder);
      $files = scandir($folder);
      foreach ($files as $file) {
        if ($file !== '.' && $file !== '..') {
          $filePath = $folderName . '/' . $file;
          $fileType = mime_content_type($directory . '/' . $folderName . '/' . $file);
          $previewIcon = getPreviewIcon($fileType);
          $previewAction = getPreviewAction($fileType, $directory . $filePath);

          echo '<tr>';
          echo '<td>' . $file . '</td>';
          echo '<td>' . $folderName . '</td>';
          echo '<td><i class="' . $previewIcon . '"></i></td>';
          echo '<td>';
          echo $previewAction;
          echo '<a href="hapus.php?file=' . urlencode($filePath) . '" class="btn btn-danger-custom"><i class="fas fa-trash"></i> Hapus</a> ';
          echo '<button class="btn btn-primary-custom" data-toggle="modal" data-target="#changeFileModal" data-file="' . $filePath . '"><i class="fas fa-exchange-alt"></i> Ganti</button>';
          echo '</td>';
          echo '</tr>';
        }
      }
    }

    function getPreviewIcon($fileType) {
      if (strpos($fileType, 'image') !== false) {
        return 'fas fa-eye';
      } elseif (strpos($fileType, 'pdf') !== false) {
        return 'fas fa-file-pdf';
      } elseif (strpos($fileType, 'msword') !== false || strpos($fileType, 'vnd.openxmlformats-officedocument.wordprocessingml.document') !== false) {
        return 'fas fa-file-word';
      } else {
        return 'fas fa-file';
      }
    }

    function getPreviewAction($fileType, $filePath) {
      $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);

      if (strpos($fileType, 'image') !== false) {
        return '<a href="' . $filePath . '" target="_blank" class="btn btn-primary-custom"><i class="fas fa-eye"></i> Lihat</a> ';
      } elseif ($fileExtension === 'pdf') {
        return '<a href="' . $filePath . '" target="_blank" class="btn btn-primary-custom"><i class="fas fa-eye"></i> Lihat</a> ';
      } elseif (in_array($fileExtension, ['doc', 'docx'])) {
        return '<a href="view_word.php?file=' . urlencode($filePath) . '" target="_blank" class="btn btn-primary-custom"><i class="fas fa-eye"></i> Lihat</a> ';
      } else {
        return '<a href="' . $filePath . '" download class="btn btn-primary-custom"><i class="fas fa-download"></i> Unduh</a> ';
      }
    }

    ?>
  </tbody>
</table>

<!-- Modal Ganti File -->
<!-- Modal Ganti File -->
<div class="modal fade" id="changeFileModal" tabindex="-1" role="dialog" aria-labelledby="changeFileModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="" method="POST" enctype="multipart/form-data">
        <div class="modal-header">
          <h5 class="modal-title" id="changeFileModalLabel">Ganti File</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="new_file">Pilih File Baru:</label>
            <input type="file" name="new_file" id="new_file" class="form-control-file" required>
          </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="current_file" id="current_file">
          <input type="submit" name="change_file" value="Ganti" class="btn btn-primary-custom">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php

// Fungsi Ganti File
if (isset($_POST['change_file'])) {
  $newFile = $_FILES['new_file'];
  $newFileName = $newFile['name'];
  $newFileTmp = $newFile['tmp_name'];
  $newFileSize = $newFile['size'];
  $newFileError = $newFile['error'];
  $currentFile = $_POST['current_file'];

  if ($newFileError === 0) {
    $destination = $directory . $folder . '/' . $currentFile;
    move_uploaded_file($_FILES['new_file']['tmp_name'], $destination);
    echo '<div class="alert alert-success">File berhasil diganti.</div>';
} else {
    echo '<div class="alert alert-danger">Terjadi kesalahan saat mengganti file.</div>';
}

}
?>

<script>
  // Update modal data saat modal ditampilkan
  $('#changeFileModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var file = button.data('file');
    var modal = $(this);
    modal.find('.modal-body #current_file').val(file);
  });
</script>
```

# 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
Antarmuka Pengguna Grafis (Graphical User Interface atau GUI) mengacu pada elemen visual dan komponen interaktif dari aplikasi perangkat lunak atau produk digital yang memungkinkan pengguna untuk berinteraksi dengannya. Dalam kode yang diberikan, GUI dari produk digital "Manajemen File" dapat dijelaskan sebagai berikut:

1. Header: Bagian header berisi bilah navigasi di bagian atas, yang mencakup nama merek "Google Drive" dan tombol menu yang dapat diklik. Item menu diatur di sebelah kanan dan mencakup tautan untuk berbagai tindakan seperti "Upload", "Daftar File", "Buat Folder", "Hapus Folder", dan "Log out".

2. Konten Utama: Area konten utama terdapat dalam wadah dengan gaya yang disesuaikan. Ini mencakup bagian berikut:

   a. Formulir "Upload File": Formulir ini memungkinkan pengguna untuk memilih file dari perangkat mereka dan memilih folder tujuan dari menu dropdown. Terdapat tombol "Upload" untuk memulai proses pengunggahan file.

   b. Tabel "Daftar File": Tabel ini menampilkan daftar file dalam berbagai folder. Tabel ini memiliki empat kolom: "Nama File" (nama file), "Folder" (nama folder), "Pratinjau" (ikon pratinjau yang menunjukkan jenis file), dan "Aksi" (tindakan seperti melihat, menghapus, dan mengganti file). Setiap baris mewakili sebuah file yang disimpan dalam folder tertentu.

   c. Modal "Ganti File": Modal ini ditampilkan ketika pengguna mengklik tombol "Ganti" untuk sebuah file. Ini memungkinkan pengguna untuk memilih file baru untuk menggantikan file yang ada. Modal ini berisi formulir dengan bidang input file dan tombol "Ganti".

   d. Bagian "Memori Used": Bagian ini menampilkan total ruang yang digunakan dalam penyimpanan file. Ini menampilkan ruang yang digunakan dalam format tertentu, seperti "2,5 GB".

   e. Formulir "Buat Folder": Formulir ini memungkinkan pengguna untuk membuat folder baru dengan memasukkan nama folder dalam bidang input teks. Terdapat tombol "Buat" untuk membuat folder.

   f. Formulir "Hapus Folder": Formulir ini memungkinkan pengguna untuk menghapus sebuah folder. Ini menampilkan menu dropdown dengan daftar nama folder yang ada, dan terdapat tombol "Hapus" untuk memulai proses penghapusan folder.

3. Footer: Bagian footer ditempatkan di bagian bawah dan mencakup informasi hak cipta untuk produk "Manajemen File".

Secara keseluruhan, antarmuka pengguna menyediakan antarmuka yang ramah pengguna untuk mengelola file, memungkinkan pengguna untuk mengunggah file, melihat rincian file, melakukan tindakan pada file (seperti melihat, menghapus, dan mengganti), mengelola folder, dan memeriksa ruang penyimpanan yang digunakan. Desain mengikuti tata letak yang bersih dan responsif menggunakan kerangka kerja CSS Bootstrap, sehingga antarmuka ini menarik secara visual dan mudah dinavigasi.
# 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
Dalam kode diatas, terdapat beberapa interaksi dengan server melalui protokol HTTP yang terjadi melalui antarmuka pengguna (GUI). Berikut ini adalah beberapa contoh HTTP connection yang terjadi melalui GUI dalam kode tersebut:

1. Pengunggahan File (Upload File):
   - Saat pengguna memilih file yang akan diunggah dan mengklik tombol "Upload", form pengunggahan file dikirim ke server melalui metode HTTP POST.
   - Form data yang dikirim berisi informasi tentang file yang akan diunggah dan folder tujuan.
   - Server menerima permintaan HTTP POST dan mengunggah file yang dipilih ke folder yang ditentukan.
   - Setelah pengunggahan berhasil, server mengirimkan tanggapan HTTP yang memberi tahu pengguna bahwa file telah diunggah dengan sukses, yang kemudian ditampilkan sebagai pesan sukses di antarmuka pengguna.

2. Penghapusan File (Hapus File):
   - Ketika pengguna mengklik tombol "Hapus" untuk sebuah file, permintaan HTTP GET dikirim ke server dengan parameter yang mengidentifikasi file yang akan dihapus.
   - Server menerima permintaan HTTP GET dan menghapus file yang sesuai dari folder yang ditentukan.
   - Setelah penghapusan berhasil, server mengirimkan tanggapan HTTP yang memberi tahu pengguna bahwa file telah dihapus dengan sukses, yang kemudian ditampilkan sebagai pesan sukses di antarmuka pengguna.

3. Penggantian File (Ganti File):
   - Saat pengguna mengklik tombol "Ganti" untuk sebuah file, modal untuk memilih file baru ditampilkan.
   - Setelah pengguna memilih file baru dan mengklik tombol "Ganti", form penggantian file dikirim ke server melalui metode HTTP POST.
   - Form data yang dikirim berisi informasi tentang file yang akan diganti dan file baru yang dipilih.
   - Server menerima permintaan HTTP POST dan menggantikan file yang sesuai dengan file baru yang dipilih.
   - Setelah penggantian berhasil, server mengirimkan tanggapan HTTP yang memberi tahu pengguna bahwa file telah diganti dengan sukses, yang kemudian ditampilkan sebagai pesan sukses di antarmuka pengguna.

4. Pembuatan Folder (Buat Folder):
   - Saat pengguna memasukkan nama folder baru dan mengklik tombol "Buat", form pembuatan folder dikirim ke server melalui metode HTTP POST.
   - Form data yang dikirim berisi informasi tentang nama folder baru yang akan dibuat.
   - Server menerima permintaan HTTP POST dan membuat folder baru dengan nama yang ditentukan.
   - Setelah pembuatan folder berhasil, server mengirimkan tanggapan HTTP yang memberi tahu pengguna bahwa folder telah dibuat dengan sukses, yang kemudian ditampilkan sebagai pesan sukses di antarmuka pengguna.

Dengan menggunakan HTTP, interaksi antara antarmuka pengguna dan server memungkinkan pengguna untuk melakukan berbagai tindakan seperti pengunggahan, penghapusan, dan penggantian file, serta pembuatan folder. Server merespons permintaan HTTP dan memberikan tanggapan yang diperlukan kepada pengguna untuk menginformasikan tentang keberhasilan atau kegagalan tindakan yang dilakukan. Pesan sukses atau pesan kesalahan yang diterima dari server kemudian ditampilkan di antarmuka pengguna untuk memberikan umpan balik kepada pengguna tentang hasil dari tindakan yang dilakukan.
# 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

https://youtu.be/U307eJdxEWo
